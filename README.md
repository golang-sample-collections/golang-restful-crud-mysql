# Golang-Restful-CRUD-with-MYSQL-Database

Sample Go Language for CRUD with restful service API

### Installation

1. Download clone repo by this command : `git clone https://gitlab.com/golang-sample-collections/golang-restful-crud-mysql.git` 
3. `cd golang-restful-crud-mysql`
2. Edit your database connection on this code:
    * sql.Open("mysql", "root@tcp(127.0.0.1:3306)/db-example")
    * sql.Open("mysql", "username:password@tcp(yourIpAddress:port)/yourDatabase")
3. `go get github.com/gin-gonic/gin` -> Gin is a web framework written in Go (Golang).
4. `go get github.com/go-sql-driver/mysql` -> Mysql driver for Go
5. Special for BCrypt use : `git clone https://go.googlesource.com/crypto`
6. Run using this command -> `go run main.go` Or use command -> `go build main.go` to generate executable file.

### Listening and serving HTTP on :8000
```go
    GET /api/user/:id --> main.getById
    GET /api/users --> main.getAll
    POST /api/user --> main.add
    PUT /api/user/:id --> main.update
    DELETE /api/user/:id --> main.delete
```

Source :
1. [Go Languange](https://golang.org)
2. [Gin Framework](https://gin-gonic.github.io/gin/)
3. [MySQL Database](https://www.mysql.com/)

Happy Coding!
